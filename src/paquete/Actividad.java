package paquete;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;

public class Actividad {

	protected Shell shell;
	private Text text;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Actividad window = new Actividad();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(450, 300);
		shell.setText("SWT Application");
		
		Label lblNombre = new Label(shell, SWT.NONE);
		lblNombre.setBounds(10, 10, 55, 15);
		lblNombre.setText("Nombre");
		
		text = new Text(shell, SWT.BORDER);
		text.setBounds(84, 10, 76, 21);
		
		Button btnBoton = new Button(shell, SWT.NONE);
		btnBoton.setBounds(176, 10, 75, 25);
		btnBoton.setText("Boton");

	}
}
